provider "aws" {
  region  = "us-west-2"
  profile = "cpd-tools"
  version = "~> 1.1"
}

provider "aws" {
  region  = "ap-south-1"
  profile = "cpd-tools"
  version = "~> 1.1"
  alias = "ap-south-1"
}

provider "rancher" {
  api_url    = "https://rancher3.biw-services.com"
  access_key = "${var.rancher_api_access_key}"
  secret_key = "${var.rancher_api_secret_key}"
  version    = "~> 1.1"
}

terraform {
  backend "s3" {
    bucket  = "biw-cpd-tools-terraform-state"
    key     = "gitlab/main.tfstate"
    profile = "cpd-tools"
    region  = "us-west-2"
  }
}