variable "application" {
  default = "gitlab"
}
variable "aws_region" {
  default = "us-west-2"
}

# from tfvars file
variable "db_pass" {}
variable "rancher_api_access_key" {}
variable "rancher_api_secret_key" {}
