        # tell gitlab to use aurora
        postgresql['enable'] = false
        gitlab_rails['db_adapter'] = 'postgresql'
        gitlab_rails['db_encoding'] = 'utf8'
        gitlab_rails['db_host'] = '${db_endpoint}'
        gitlab_rails['db_port'] = '${db_port}'
        gitlab_rails['db_username'] = '${db_user}'
        gitlab_rails['db_password'] = '${db_password}'
        gitlab_rails['db_database'] = '${db_name}'
        # elasticache / redis
        gitlab_rails['redis_host'] = "${redis_host}"
        gitlab_rails['redis_port'] = ${redis_port}
        # telling gitlab that ssl is terminated before it gets to this container
        nginx['listen_port'] = 80
        nginx['listen_https'] = false
        nginx['proxy_set_headers'] = {
          "X-Forwarded-Proto" => "https",
          "X-Forwarded-Ssl" => "on"
        }
        # consolidating NFS mount points
        git_data_dirs({"default" => "/gitlab-data/git-data"})
        user['home'] = '/gitlab-data/home'
        gitlab_rails['uploads_directory'] = '/gitlab-data/uploads'
        gitlab_rails['shared_path'] = '/gitlab-data/shared'
        gitlab_ci['builds_directory'] = '/gitlab-data/builds'
        gitlab_rails['pages_path'] = "/gitlab-data/pages"
        # github enterprise configured as a provider
        gitlab_rails['omniauth_providers'] = [
          {
            "name" => "github",
            "url" => "https://github.biworldwide.com/",
            "verify_ssl" => false
          }
        ]
        # tell gitlab to _always_ ignore the ssl verification
        omnibus_gitconfig['system'] = { "http" => ["sslVerify = false"] }
        # SMTP configuration
        gitlab_rails['smtp_enable'] = true
        gitlab_rails['smtp_address'] = "email-smtp.us-west-2.amazonaws.com"
        gitlab_rails['smtp_port'] = 2587
        gitlab_rails['smtp_user_name'] = "${smtp_user_name}"
        gitlab_rails['smtp_password'] = "${smtp_password}"
        gitlab_rails['smtp_domain'] = "${smtp_domain}"
        gitlab_rails['smtp_authentication'] = "${smtp_authentication}"        
        # gitlab_rails['gitlab_email_from'] = 'adam.dehnel@biworldwide.com'
        # gitlab_rails['gitlab_email_reply_to'] = 'adam.dehnel@biworldwide.com'
        gitlab_rails['ldap_enabled'] = true
        gitlab_rails['ldap_servers'] = YAML.load <<-EOS # remember to close this block with 'EOS' below
        main: # 'main' is the GitLab 'provider ID' of this LDAP server
          ## label
          #
          # A human-friendly name for your LDAP server. It is OK to change the label later,
          # for instance if you find out it is too large to fit on the web page.
          #
          # Example: 'Paris' or 'Acme, Ltd.'
          label: 'LDAP'

          # Example: 'ldap.mydomain.com'
          # host: 'biusdc8.bius.bi.corp'
          host: '172.16.93.16'
          # This port is an example, it is sometimes different but it is always an integer and not a string
          port: 636 # 389 / usually 636 for SSL
          uid: 'sAMAccountName' # This should be the attribute, not the value that maps to uid.

          # Examples: 'america\\momo' or 'CN=Gitlab Git,CN=Users,DC=mydomain,DC=com'
          bind_dn: 'svc-ldap-github'
          password: 'H7y!8vtRH6&cz8'

          # Encryption method. The "method" key is deprecated in favor of
          # "encryption".
          #
          #   Examples: "start_tls" or "simple_tls" or "plain"
          #
          #   Deprecated values: "tls" was replaced with "start_tls" and "ssl" was
          #   replaced with "simple_tls".
          #
          encryption: 'plain'

          # Enables SSL certificate verification if encryption method is
          # "start_tls" or "simple_tls". Defaults to true since GitLab 10.0 for
          # security. This may break installations upon upgrade to 10.0, that did
          # not know their LDAP SSL certificates were not setup properly. For
          # example, when using self-signed certificates, the ca_file path may
          # need to be specified.
          verify_certificates: false

          # Specifies the path to a file containing a PEM-format CA certificate,
          # e.g. if you need to use an internal CA.
          #
          #   Example: '/etc/ca.pem'
          #
          # ca_file: ''

          # Specifies the SSL version for OpenSSL to use, if the OpenSSL default
          # is not appropriate.
          #
          #   Example: 'TLSv1_1'
          #
          # ssl_version: ''

          # Set a timeout, in seconds, for LDAP queries. This helps avoid blocking
          # a request if the LDAP server becomes unresponsive.
          # A value of 0 means there is no timeout.
          timeout: 60

          # This setting specifies if LDAP server is Active Directory LDAP server.
          # For non AD servers it skips the AD specific queries.
          # If your LDAP server is not AD, set this to false.
          active_directory: true

          # If allow_username_or_email_login is enabled, GitLab will ignore everything
          # after the first '@' in the LDAP username submitted by the user on login.
          #
          # Example:
          # - the user enters 'jane.doe@example.com' and 'p@ssw0rd' as LDAP credentials;
          # - GitLab queries the LDAP server with 'jane.doe' and 'p@ssw0rd'.
          #
          # If you are using "uid: 'userPrincipalName'" on ActiveDirectory you need to
          # disable this setting, because the userPrincipalName contains an '@'.
          allow_username_or_email_login: false

          # To maintain tight control over the number of active users on your GitLab installation,
          # enable this setting to keep new users blocked until they have been cleared by the admin
          # (default: false).
          block_auto_created_users: false

          # Base where we can search for users
          #
          #   Ex. 'ou=People,dc=gitlab,dc=example' or 'DC=mydomain,DC=com'
          #
          base: ''

          # Filter LDAP users
          #
          #   Format: RFC 4515 https://tools.ietf.org/search/rfc4515
          #   Ex. (employeeType=developer)
          #
          #   Note: GitLab does not support omniauth-ldap's custom filter syntax.
          #
          #   Example for getting only specific users:
          #   '(&(objectclass=user)(|(samaccountname=momo)(samaccountname=toto)))'
          #
          user_filter: ''

          # LDAP attributes that GitLab will use to create an account for the LDAP user.
          # The specified attribute can either be the attribute name as a string (e.g. 'mail'),
          # or an array of attribute names to try in order (e.g. ['mail', 'email']).
          # Note that the user's LDAP login will always be the attribute specified as `uid` above.
          attributes:
            # The username will be used in paths for the user's own projects
            # (like `gitlab.example.com/username/project`) and when mentioning
            # them in issues, merge request and comments (like `@username`).
            # If the attribute specified for `username` contains an email address,
            # the GitLab username will be the part of the email address before the '@'.
            username: ['uid', 'userid', 'displayName', 'sAMAccountName']
            email:    ['mail', 'email', 'userPrincipalName']

            # If no full name could be found at the attribute specified for `name`,
            # the full name is determined using the attributes specified for
            # `first_name` and `last_name`.
            name:       'cn'
            first_name: 'givenName'
            last_name:  'sn'

          ## EE only

          # Base where we can search for groups
          #
          #   Ex. ou=groups,dc=gitlab,dc=example
          #
          group_base: ''

          # The CN of a group containing GitLab administrators
          #
          #   Ex. administrators
          #
          #   Note: Not `cn=administrators` or the full DN
          #
          admin_group: 'github-access'

          # The LDAP attribute containing a user's public SSH key
          #
          #   Ex. ssh_public_key
          #
          sync_ssh_keys: false

        # GitLab EE only: add more LDAP servers
        # Choose an ID made of a-z and 0-9 . This ID will be stored in the database
        # so that GitLab can remember which LDAP server a user belongs to.
        # uswest2:
        #   label:
        #   host:
        #   ....
        EOS
