resource "aws_iam_user" "gitlab_user" {
  name = "gitlab-user"
}

resource "aws_iam_access_key" "gitlab_user" {
  user = "${aws_iam_user.gitlab_user.name}"
}

data "aws_iam_policy_document" "gitlab_user_policy" {
  statement {
    actions = [
      "es:*"
    ]

    resources = [
      "${aws_elasticsearch_domain.main.arn}",
      "${aws_elasticsearch_domain.main.arn}/*",
    ]
  }
  statement {
    actions = [
      "ses:SendRawEmail"
    ]

    resources = [
      "*"
    ]
  }
}

resource "aws_iam_policy" "gitlab_user_policy" {
  name   = "gitlab-user-policy"
  path   = "/"
  policy = "${data.aws_iam_policy_document.gitlab_user_policy.json}"
}

resource "aws_iam_policy_attachment" "gitlab_user_policy_attachment" {
  name = "gitlab-user-policy-attachment"

  users      = ["${aws_iam_user.gitlab_user.name}"]
  policy_arn = "${aws_iam_policy.gitlab_user_policy.arn}"
}
