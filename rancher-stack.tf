resource "rancher_stack" "gitlab" {
  name            = "gitlab"
  environment_id  = "1a129"
  start_on_create = true
  finish_upgrade  = true
  docker_compose  = "${data.template_file.docker_compose.rendered}"
  rancher_compose = "${file("${path.module}/rancher/rancher-compose.yml")}"
}

data "template_file" "docker_compose" {
  template = "${file("${path.module}/rancher/docker-compose.yml")}"

  vars {
    gitlab_host         = "gitlab.biw-services.com"
    gitlab_config = "${data.template_file.gitlab_rb.rendered}"
    db_password         = "${var.db_pass}"
    db_endpoint         = "${aws_rds_cluster.gitlab.endpoint}"
    db_port             = "5432"
    db_name             = "gitlab"
    db_user             = "gitlab"
  }
}

data "template_file" "gitlab_rb" {
  template = "${file("${path.module}/rancher/gitlab.rb")}"

  vars {
    db_password         = "${var.db_pass}"
    db_endpoint         = "${aws_rds_cluster.gitlab.endpoint}"
    db_port             = "5432"
    db_name             = "gitlab"
    db_user             = "gitlab"
    gitlab_host         = "gitlab.biw-services.com"
    redis_host          = "${aws_elasticache_replication_group.elasticache.primary_endpoint_address}"
    redis_port          = "${aws_elasticache_replication_group.elasticache.port}"
    smtp_user_name      = "${aws_iam_access_key.gitlab_user.id}"
    smtp_password       = "${aws_iam_access_key.gitlab_user.ses_smtp_password}"
    smtp_domain         = "gitlab.biw-services.com"
    smtp_authentication = "login"
  }
}