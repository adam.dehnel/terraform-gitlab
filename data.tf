data "terraform_remote_state" "cpd_tools_shared" {
  backend = "s3"

  config {
    bucket  = "biw-cpd-tools-terraform-state"
    key     = "shared.tfstate"
    region  = "us-west-2"
    profile = "cpd-tools"
  }
}
