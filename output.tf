output "gitlab_iam_user_access_key" {
  value = "${aws_iam_access_key.gitlab_user.id}"
}

output "gitlab_iam_user_secret_key" {
  value = "${aws_iam_access_key.gitlab_user.secret}"
}
