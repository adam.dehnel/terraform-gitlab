resource "aws_elasticache_parameter_group" "elasticache" {
  name   = "${var.application}-elasticache-parameters"
  family = "redis3.2"

  parameter {
    name  = "activerehashing"
    value = "yes"
  }

  parameter {
    name  = "min-slaves-to-write"
    value = "2"
  }
}

resource "aws_elasticache_subnet_group" "elasticache" {
  name       = "${var.application}-elasticache-subnet-group"
  subnet_ids = ["${data.terraform_remote_state.cpd_tools_shared.private_prod_subnet_ids}"]
}

resource "aws_elasticache_replication_group" "elasticache" {
  replication_group_id          = "${var.application}-elasticache"
  replication_group_description = "${var.application} Elasticache Replication Group"
  node_type                     = "cache.m3.medium"
  number_cache_clusters         = 3
  port                          = 6379
  parameter_group_name          = "${aws_elasticache_parameter_group.elasticache.name}"
  availability_zones            = [ "us-west-2a", "us-west-2b", "us-west-2c" ]
  automatic_failover_enabled    = true
  security_group_ids            = [ "${aws_security_group.elasticache.id}" ]
  subnet_group_name             = "${aws_elasticache_subnet_group.elasticache.name}"
}

resource "aws_security_group" "elasticache" {
  name        = "${var.application}-elasticache-secgroup"
  vpc_id      = "${data.terraform_remote_state.cpd_tools_shared.vpc_id}"

  ingress {
    from_port = 6379
    to_port   = 6379
    protocol  = "tcp"

    # cidr_blocks = ["${var.vpc_cidr}"]
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    application = "${var.application}"
  }
}
