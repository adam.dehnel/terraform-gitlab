resource "aws_elasticsearch_domain" "main" {
  domain_name           = "${var.application}"
  elasticsearch_version = "5.3"

  ebs_options {
    ebs_enabled = true
    volume_size = 10
  }

  cluster_config {
    instance_type = "t2.small.elasticsearch"
  }

  snapshot_options {
    automated_snapshot_start_hour = 23
  }

  tags {
    application = "${var.application}"
  }
}

resource "aws_elasticsearch_domain_policy" "main" {
  domain_name     = "${aws_elasticsearch_domain.main.domain_name}"
  access_policies = "${data.aws_iam_policy_document.es_access_policy.json}"
}

data "aws_iam_policy_document" "es_access_policy" {
  statement {
    principals {
      type        = "AWS"
      identifiers = ["${aws_iam_user.gitlab_user.arn}"]
    }

    actions = [
      "es:*",
    ]

    resources = [
      "${aws_elasticsearch_domain.main.arn}",
      "${aws_elasticsearch_domain.main.arn}/*",
    ]
  }
}
