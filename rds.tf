#------------------------------------------#
# RDS Database Configuration
#------------------------------------------#
resource "aws_rds_cluster_instance" "gitlab" {
  count                = 2
  identifier           = "${var.application}-db-${count.index}"
  cluster_identifier   = "${aws_rds_cluster.gitlab.id}"
  instance_class       = "db.r4.large"
  engine               = "aurora-postgresql"
  publicly_accessible  = true
  db_subnet_group_name = "${aws_db_subnet_group.gitlab.name}"

  tags {
    application = "${var.application}"
  }
}

resource "aws_rds_cluster" "gitlab" {
  cluster_identifier     = "${var.application}-db"
  engine                 = "aurora-postgresql"
  database_name          = "gitlab"
  master_username        = "gitlab"
  master_password        = "${var.db_pass}"
  db_subnet_group_name   = "${aws_db_subnet_group.gitlab.name}"
  vpc_security_group_ids = [
    "${aws_security_group.gitlab_rds.id}",
    "${data.terraform_remote_state.cpd_tools_shared.allow_biw_traffic_security_group_id}"
  ]
  final_snapshot_identifier = "${var.application}-db-final"
  storage_encrypted         = true

  tags {
    application = "${var.application}"
  }
}

resource "aws_db_subnet_group" "gitlab" {
  name        = "${var.application}-db-subnet-group"
  subnet_ids  = ["${data.terraform_remote_state.cpd_tools_shared.private_prod_subnet_ids}"]

  tags {
    Name        = "${var.application}-db-subnet-group"
    application = "${var.application}"
  }
}

resource "aws_security_group" "gitlab_rds" {
  name        = "${var.application}-rds-secgroup"
  vpc_id      = "${data.terraform_remote_state.cpd_tools_shared.vpc_id}"

  ingress {
    from_port = 0
    to_port   = 65535
    protocol  = "tcp"
    self      = true
  }

  ingress {
    from_port = 0
    to_port   = 65535
    protocol  = "udp"
    self      = true
  }

  ingress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"

    # cidr_blocks = ["${var.vpc_cidr}"]
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    application = "${var.application}"
  }
}
